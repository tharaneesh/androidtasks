package com.example.cpsample

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    var e1: EditText? = null
    var e2: EditText? = null
    var nextButton: Button? = null
    var prevButton: Button? = null
    var insertButton: Button? = null
    var UpdateButton: Button? = null
    var DeleteButton: Button? = null
    var clearButton: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nextButton = findViewById(R.id.button_next)
        prevButton = findViewById(R.id.button_prev)
        insertButton = findViewById(R.id.button_insert)
        UpdateButton = findViewById(R.id.button_updat)
        DeleteButton = findViewById(R.id.button_delete)
        clearButton = findViewById(R.id.button_clear)
        e1 = findViewById(R.id.editTextTextPersonName)
        e2 = findViewById(R.id.editTextTextPersonName2)

        val rs = contentResolver.query(
            MyContentProvider.CONTENT_URI,
            arrayOf(MyContentProvider._ID, MyContentProvider.NAME,MyContentProvider.MEANING),
            null,
            null,
            MyContentProvider.NAME
        )
        println(rs)
        nextButton!!.setOnClickListener {
            if (rs?.moveToNext()!!) {
                e1!!.setText(rs.getString(1))
                e2!!.setText(rs.getString(2))
            }
        }
        prevButton!!.setOnClickListener {
            if (rs?.moveToPrevious()!!) {
                e1!!.setText(rs.getString(1))
                e2!!.setText(rs.getString(2))
            }
        }
        clearButton!!.setOnClickListener {
            e1!!.setText("")
            e2!!.setText("")
            e1!!.requestFocus()
        }
        insertButton!!.setOnClickListener {
            var cv = ContentValues()
            cv.put(MyContentProvider.NAME, e1!!.text.toString())
            cv.put(MyContentProvider.MEANING, e2!!.text.toString())
            contentResolver.insert(MyContentProvider.CONTENT_URI, cv)
            rs?.requery()
        }
        UpdateButton!!.setOnClickListener {
            var cv = ContentValues()
            cv.put(MyContentProvider.MEANING, e2!!.text.toString())
            contentResolver.update(
                MyContentProvider.CONTENT_URI,
                cv,
                "NAME = ?",
                arrayOf(e1!!.text.toString())
            )
            rs?.requery()

        }
        DeleteButton!!.setOnClickListener {
            contentResolver.delete(
                MyContentProvider.CONTENT_URI,
                "NAME = ?",
                arrayOf(e1!!.text.toString())
            )
            rs?.requery()
        }


    }
}