package com.example.cpsample

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class MyHelper(context: Context): SQLiteOpenHelper(context,"MyTable",null,1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL("CREATE TABLE EMP(_id INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT,MEANING TEXT)")
        db!!.execSQL("INSERT INTO EMP(NAME,MEANING) VALUES('ABCD','XYZ')")
        db!!.execSQL("INSERT INTO EMP(NAME,MEANING) VALUES('Tharaneesh','Software Trainee')")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
       // db?.execSQL("drop table if exists"+"EMP")
    }
}