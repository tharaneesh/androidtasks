package com.example.cpsample

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri

class MyContentProvider : ContentProvider() {


    companion object{
        var PROVIDER_NAME = "com.example.check"
        val URL = "content://$PROVIDER_NAME/EMP"
        val CONTENT_URI = Uri.parse(URL)
        val _ID = "_id"
        val NAME = "NAME"
        val MEANING = "MEANING"
    }
    lateinit var db: SQLiteDatabase

    override fun onCreate(): Boolean {
        val helper = MyHelper(context!!)
        db=helper.writableDatabase
        return if(db==null) false
        else true
    }
    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        db.insert("EMP",null,values)
        context?.contentResolver?.notifyChange(uri,null)
        return uri
    }
    override fun update(
        uri: Uri, values: ContentValues?, selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        var count = db.update("EMP",values,selection,selectionArgs)
        context?.contentResolver?.notifyChange(uri,null)
        return count
    }
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        var count:Int = db.delete("EMP",selection,selectionArgs)
        context?.contentResolver?.notifyChange(uri,null)
        return count
    }

    override fun getType(uri: Uri): String? {
        return "vnd.android.cursor.dir/vnd.example.EMP"
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        return db.query("EMP",projection,selection,selectionArgs,null,null,null)
    }


}