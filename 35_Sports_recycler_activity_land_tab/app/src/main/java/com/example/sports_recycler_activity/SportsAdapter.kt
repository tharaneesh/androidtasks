package com.example.sports_recycler_activity

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.sports_recycler_activity.model.SportsPost
import kotlinx.android.synthetic.main.layout_list_item.view.*
import java.util.*
import kotlin.collections.ArrayList



class SportsAdapter(context: Context, sports_list: ArrayList<SportsPost>):RecyclerView.Adapter<RecyclerView.ViewHolder>() {
     var items: MutableList<SportsPost> = ArrayList()
      var mContext: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SportsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    when(holder){
        is SportsViewHolder -> {
            holder.bind(items.get(position))
        }
    }

    }
    init{
        this.items = sports_list
        mContext = context
    }
    fun submitList(sportsList: List<SportsPost>){
        items= sportsList as MutableList<SportsPost>
    }
    override fun getItemCount(): Int {
        return items.size
    }

   fun deleteItem(position: Int){
        items.removeAt(position)
        notifyDataSetChanged()
    }
   inner class SportsViewHolder constructor(itemView: View):
            RecyclerView.ViewHolder(itemView) {


        val sportsImage = itemView.sports_image
        val sportsTitle = itemView.sports_title
        val sportsBody = itemView.sports_body
        val sportsNote = itemView.sports_note



       init {
           itemView.setOnClickListener {
               val position=adapterPosition
               println("test")
               val currentSport: SportsPost = items[position]
               val intent=Intent(itemView.context,DetailActivity::class.java)
               intent.putExtra("title",currentSport.title)
               intent.putExtra("image_",currentSport.image)
               itemView.context.startActivity(intent)
           }
       }
    //    override fun onClick(v: View?) {
  //      println("test")
        //    val currentSport: SportsPost = items.get(getAdapterPosition())
          //  val detailIntent = Intent(mContext, DetailActivity::class.java)
           // detailIntent.putExtra("title", "sportsTitle")
//            detailIntent.putExtra(
//                "image_resource",
//                "images"
//            )
      //      mContext!!.startActivity(detailIntent)
   //     }
    fun bind(sportsPost: SportsPost) {
        sportsTitle.setText(sportsPost.title)
        sportsBody.setText(sportsPost.body)
        sportsNote.setText(sportsPost.note)

        val reqOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
        Glide.with(itemView.context)
            .applyDefaultRequestOptions(reqOptions)
            .load(sportsPost.image).into(sportsImage)

    }


    }


}