package com.example.sports_recycler_activity.model

data class SportsPost(
    var title: String,
    var image: String,
    var body: String,
    var note: String){
}