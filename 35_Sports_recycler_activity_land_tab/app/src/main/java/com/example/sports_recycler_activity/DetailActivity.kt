package com.example.sports_recycler_activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val sportsTitle = findViewById<TextView>(R.id.titleDetail)
        val sportsImage: ImageView = findViewById(R.id.sportsImageDetail)
        val data = DataSource.createDataSet()

       sportsTitle.text = intent.getStringExtra("title")
      //  sportsTitle.text = "hello"

        val reqOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)

        Glide.with(this).applyDefaultRequestOptions(reqOptions).load(intent.getStringExtra("image_"))
            .into(sportsImage)
    }
}