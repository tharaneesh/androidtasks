package com.example.alarmclock

import android.app.*
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.text.Editable
import android.view.View
import android.widget.EditText
import android.widget.TimePicker
import android.widget.Toast
import java.util.*


class MainActivity : AppCompatActivity(),TimePickerDialog.OnTimeSetListener {
    var mHour:EditText?=null
    lateinit var mMinute:EditText
    lateinit var calendar: Calendar
  //  lateinit var timePickerDialog: TimePickerDialog
    var currentHour=0
     var currentMin=0
    var savedHour = 0
    var savedMin = 0
    var mNotificationManager:NotificationManager?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mHour=findViewById(R.id.hour)
        mMinute=findViewById(R.id.minute)
        createNotificationChannel()
    }

    private fun getTimeCalendar() {

        calendar = Calendar.getInstance()
        currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        currentMin= calendar.get(Calendar.MINUTE)

    }


    fun setTime(view: View) {
        getTimeCalendar()

     TimePickerDialog(this,this,currentHour,currentMin,true).show()
       }



    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay)
        calendar.set(Calendar.MINUTE,minute)
        calendar.set(Calendar.SECOND,0)
        savedHour=hourOfDay
        savedMin=minute

        mHour!!.setText("Hour :" + savedHour.toString())
        mMinute.setText("Minute:" + savedMin.toString())

    }

    fun setAlarm(view: View) {

        if (savedHour != 0 && savedMin != 0) {
            val intent: Intent
            intent = Intent(AlarmClock.ACTION_SET_ALARM)
            intent.putExtra(AlarmClock.EXTRA_HOUR, savedHour)
            intent.putExtra(AlarmClock.EXTRA_MINUTES, savedMin)
            intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Morning Walk")
            startActivity(intent)
        } else {
            Toast.makeText(this, "Set the time", Toast.LENGTH_LONG).show()
        }

    }


    fun createNotificationChannel() {

        mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.O
        ) {

            val notificationChannel = NotificationChannel(
                "primary_notification_channelll",
                "Stand up notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.description = "Notifies every 15 minutes to " +
                    "stand up and walk"
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
    }

    fun setNotificaton(view: View) {
        if (savedHour != 0 && savedMin != 0) {
            Toast.makeText(this, "Notification On", Toast.LENGTH_LONG).show()
            val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
            val intent = Intent(this, MyReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0)
            //time interval
            // val currentTime = System.currentTimeMillis()
            //val schTime = 1000*10
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)


            val currentTime = System.currentTimeMillis()
            val schTime = 1000*10
            val pendingIntent2 = PendingIntent.getBroadcast(this, 0, intent, 0)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,currentTime+schTime , pendingIntent2)

            mHour!!.setText("")
            mMinute.setText("")
        }
        else {
            Toast.makeText(this, "Set the time", Toast.LENGTH_LONG).show()
        }
    }
    fun cancelAlarm(view: View) {
        Toast.makeText(this, "Notification Off(Specified Time)", Toast.LENGTH_LONG).show()
        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(this,MyReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this,1,intent,0)
        alarmManager.cancel(pendingIntent)
    }

}

