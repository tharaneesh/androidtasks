package com.example.alarmclock

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        var mNotificationManager = NotificationManagerCompat.from(context)

        val builder = NotificationCompat.Builder(context,"primary_notification_channel")
        builder.setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Alarm Notification")
            .setContentText("Wake up")



        mNotificationManager.notify(1,builder.build())
    }
}