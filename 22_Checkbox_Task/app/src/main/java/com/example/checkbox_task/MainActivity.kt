package com.example.checkbox_task

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    var choc: CheckBox? = null
    var sprinkles:CheckBox? = null
    var cNuts:CheckBox? = null
    var oreo:CheckBox? = null
    var cherries:CheckBox? = null
    var buttonOrder: Button? = null
    var result:SpannableStringBuilder= SpannableStringBuilder("")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        choc = findViewById(R.id.checkBox)
        sprinkles = findViewById(R.id.checkBox2)
        cNuts = findViewById(R.id.checkBox3)
        cherries = findViewById(R.id.checkBox4)
        oreo = findViewById(R.id.checkBox5)
        // buttonOrder=findViewById(R.id.button_show)

    }
    fun display(view: View) {

        if(choc!!.isChecked) {
            result.append(choc!!.text.toString()+" ")
        }
        if(sprinkles!!.isChecked) {
            result.append(sprinkles!!.text.toString()+" ")
        }
        if(cNuts!!.isChecked) {
            result.append(cNuts!!.text.toString()+" ")
        }
        if(cherries!!.isChecked) {
            result.append(cherries!!.text.toString()+" ")
        }
        if(oreo!!.isChecked) {
            result.append(oreo!!.text.toString()+" ")
        }
        Log.i("Main",result.toString())
        Toast.makeText(this,result.toString(),Toast.LENGTH_SHORT).show()
        result.clear()
    }


}