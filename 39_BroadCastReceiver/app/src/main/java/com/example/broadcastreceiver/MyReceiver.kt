package com.example.broadcastreceiver

import android.R
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast


class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val intentAction = intent.action
        if (intentAction != null) {
            var toastMessage = "Unknown error"
            when (intentAction) {
                Intent.ACTION_POWER_CONNECTED -> toastMessage = "Power connected"
                Intent.ACTION_POWER_DISCONNECTED -> toastMessage = "Power Disconnected"
                ACTION_CUSTOM_BROADCAST -> toastMessage = "Custom Broadcast"
            }
            Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        // String constant that defines the custom broadcast Action.
        private const val ACTION_CUSTOM_BROADCAST =
            BuildConfig.APPLICATION_ID + ".ACTION_CUSTOM_BROADCAST"
    }
}