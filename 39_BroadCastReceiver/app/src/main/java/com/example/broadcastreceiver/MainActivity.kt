package com.example.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class MainActivity : AppCompatActivity() {
    val mReceiver = MyReceiver()
   // lateinit var receiver: BroadcastReceiver
    private val ACTION_CUSTOM_BROADCAST = BuildConfig.APPLICATION_ID + ".ACTION_CUSTOM_BROADCAST"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val obj = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    //  val networkInfo = obj.activeNetworkInfo

        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED)
        filter.addAction(Intent.ACTION_POWER_CONNECTED)

    //another type - without receiver class
//        val receiver = object : BroadcastReceiver(){
//            override fun onReceive(context: Context?, intent: Intent?) {
//                Toast.makeText(context,intent!!.action,Toast.LENGTH_LONG).show()
//            }
//        }
//        if (networkInfo!= null && networkInfo.isConnected)
//        {
//            if(networkInfo.type==ConnectivityManager.TYPE_WIFI)
//            {
//                Toast.makeText(applicationContext,"Connected to wifi",Toast.LENGTH_LONG).show()
//            }
//            if(networkInfo.type==ConnectivityManager.TYPE_MOBILE)
//                Toast.makeText(applicationContext,"Connected to mobile",Toast.LENGTH_LONG).show()
//        }
//else
//        {
//            Toast.makeText(applicationContext,"You are offline",Toast.LENGTH_LONG).show()
//
//        }



        this.registerReceiver(mReceiver, filter);
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(mReceiver, IntentFilter(ACTION_CUSTOM_BROADCAST))

   }

    override fun onDestroy() {
        super.onDestroy()
        this.unregisterReceiver(mReceiver);
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(mReceiver);

    }

    fun sendCustomBroadcast(view: View) {
        val customBroadcastIntent = Intent(ACTION_CUSTOM_BROADCAST)
        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(customBroadcastIntent)
    }

}