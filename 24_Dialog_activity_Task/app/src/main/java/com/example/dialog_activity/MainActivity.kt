package com.example.dialog_activity


import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickShowAlert(view: View?) {
        val myAlertBuilder = AlertDialog.Builder(this@MainActivity)

        // Set the dialog title and message.
        myAlertBuilder.setTitle(R.string.alert_title)
        myAlertBuilder.setMessage(R.string.alert_message)

        // Add the dialog buttons.
        myAlertBuilder.setPositiveButton(R.string.ok_button,
                DialogInterface.OnClickListener { dialog, which ->
                    Toast.makeText(applicationContext,
                            R.string.pressed_ok,
                            Toast.LENGTH_SHORT).show()
                })
        myAlertBuilder.setNegativeButton(R.string.cancel_button,
                DialogInterface.OnClickListener { dialog, which ->
                    Toast.makeText(applicationContext,
                            R.string.pressed_cancel,
                            Toast.LENGTH_SHORT).show()
                })

        myAlertBuilder.show()
    }
}