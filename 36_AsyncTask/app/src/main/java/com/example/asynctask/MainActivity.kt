package com.example.asynctask

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private var mTextView: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mTextView = findViewById<View>(R.id.text_view) as TextView

        if (savedInstanceState != null) {
            mTextView!!.text = savedInstanceState.getString(TEXT_STATE)
        }
    }


    fun startTask(view: View?) {
        mTextView?.setText(R.string.napping)

        SimpleAsyncTask(mTextView).execute()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(TEXT_STATE, mTextView!!.text.toString())
    }

    companion object {
        private const val TEXT_STATE = "currentText"
    }
}
