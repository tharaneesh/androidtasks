package com.example.asynctask

import android.os.AsyncTask
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import java.lang.ref.WeakReference
import java.util.*


@Suppress("DEPRECATION")
class SimpleAsyncTask internal constructor(tv: TextView?) :
    AsyncTask<Int, Int, String>() {

    private val mTextView: WeakReference<TextView>

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun onPreExecute() {
        super.onPreExecute()


    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
    }



    override fun onPostExecute(result: String) {
        mTextView.get()?.setText(result)
    }

    init {
        mTextView = WeakReference(tv)
    }

    override fun doInBackground(vararg params: Int?): String {
        val r = Random()
        val n: Int = r.nextInt(11)


        val s = n * 200

        try {
            Thread.sleep(s.toLong())
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }


        return "Awake at last after sleeping for $s milliseconds!"}
}