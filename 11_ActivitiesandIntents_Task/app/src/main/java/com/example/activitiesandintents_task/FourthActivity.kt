package com.example.activitiesandintents_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class FourthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth)
        val button=findViewById<Button>(R.id.goback_button4)
        button.setOnClickListener {
            finish()
        }
    }
}