package com.example.activitiesandintents_task

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button=findViewById<Button>(R.id.button1)
        button.setOnClickListener {
            val intent=Intent(this,SecondActivity::class.java)
            startActivity(intent)
        }
        val button2=findViewById<Button>(R.id.button2)
        button2.setOnClickListener {
            val intent=Intent(this,ThirdMainActivity::class.java)
            startActivity(intent)
        }
        val button3=findViewById<Button>(R.id.button3)
        button3.setOnClickListener {
            val intent=Intent(this,FourthActivity::class.java)
            startActivity(intent)
        }
    }



}
