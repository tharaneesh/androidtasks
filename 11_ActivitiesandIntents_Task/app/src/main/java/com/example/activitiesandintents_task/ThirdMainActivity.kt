package com.example.activitiesandintents_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ThirdMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third_main)
        val button=findViewById<Button>(R.id.goback_button)
        button.setOnClickListener {
            finish()
        }
    }
}