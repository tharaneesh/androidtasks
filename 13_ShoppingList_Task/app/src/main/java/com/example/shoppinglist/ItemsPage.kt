package com.example.shoppinglist

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView

class ItemsPage : AppCompatActivity() {
    private var apple_hint: EditText? = null
    private var apple_item: TextView?= null
    private var cheese_hint: EditText? = null
    private var cheese_item: TextView?= null
    private var cookies_hint: EditText? = null
    private var cookies_item: TextView?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_items_page)
        apple_hint = findViewById(R.id.apple_count_hint)
        apple_item=findViewById(R.id.apple)
        cheese_item=findViewById(R.id.cheese)
        cheese_hint=findViewById(R.id.cheese_count_hint)
        cookies_item=findViewById(R.id.cookies)
        cookies_hint=findViewById(R.id.cookies_count_hint)

    }


    fun returnReply(view: View?) {
        val replyIntent = Intent()
        // Get the reply message from the edit text.
        val reply_appCount = apple_hint!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLY_APPCOUNT, reply_appCount)
        setResult(Activity.RESULT_OK,replyIntent)

        val replyitem_apple =apple_item!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLYITEM_APPLE,replyitem_apple)
        setResult(Activity.RESULT_OK,replyIntent)

        val replyitem_cheese=cheese_item!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLYITEM_CHEESE,replyitem_cheese)
        setResult(Activity.RESULT_OK,replyIntent)

        val reply_cheeseCount = cheese_hint!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLY_CHEESECOUNT, reply_cheeseCount)
        setResult(Activity.RESULT_OK,replyIntent)

        val replyitem_cookies=cookies_item!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLYITEM_COOKIES,replyitem_cookies)
        setResult(Activity.RESULT_OK,replyIntent)

        val reply_cookiesCount = cookies_hint!!.text.toString()
        replyIntent.putExtra(EXTRA_REPLY_COOKIESCOUNT, reply_cookiesCount)
        setResult(Activity.RESULT_OK,replyIntent)

        finish()
    }

    companion object {
        const val EXTRA_REPLYITEM_CHEESE = "REPLYITEM_CHEESE"
        const val EXTRA_REPLYITEM_APPLE = "REPLYITEM_APPLE"
        const val EXTRA_REPLYITEM_COOKIES = "REPLYITEM_COOKIES"
        const val EXTRA_REPLY_COOKIESCOUNT = "com.example.android.twoactivities.extra.REPLYCOOKIES"
        const val EXTRA_REPLY_APPCOUNT = "com.example.android.twoactivities.extra.REPLYAPP"
        const val EXTRA_REPLY_CHEESECOUNT = "com.example.android.twoactivities.extra.REPLYCHEESE"
    }

    fun List_column(view: View) {}

}
