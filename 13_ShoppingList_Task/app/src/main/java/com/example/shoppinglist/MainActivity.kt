package com.example.shoppinglist

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var mReplyHeadTextView: TextView? = null
    private var mReplyTextView1_1: TextView? = null
    private var mReplyTextView1_2: TextView? = null
    private var mReplyTextView2_1: TextView? = null
    private var mReplyTextView2_2: TextView? = null
    private var mReplyTextView3_1: TextView? = null
    private var mReplyTextView3_2: TextView? = null

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mReplyHeadTextView!!.visibility == View.VISIBLE) {
            outState.putBoolean("reply_visible", true)
            outState.putString("reply_text_appCount", mReplyTextView1_1!!.text.toString())
            outState.putString("reply_item_text_apple",mReplyTextView1_2!!.text.toString())
            outState.putString("reply_item_text_cheese",mReplyTextView2_1!!.text.toString())
            outState.putString("reply_cheeseCount",mReplyTextView2_2!!.text.toString())
            outState.putString("reply_item_text_cookies",mReplyTextView3_1!!.text.toString())
            outState.putString("reply_cookiesCount",mReplyTextView3_2!!.text.toString())
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //mMessageEditText = findViewById<View>(R.id.editText_main) as EditText?
        mReplyHeadTextView = findViewById<View>(R.id.text_header_reply) as TextView?
        mReplyTextView1_1= findViewById<View>(R.id.text_message_reply_count) as TextView?
        mReplyTextView1_2 = findViewById<View>(R.id.text_message_reply_1) as TextView?
        mReplyTextView2_1 = findViewById<View>(R.id.text_message_reply_2) as TextView?
        mReplyTextView2_2 = findViewById<View>(R.id.text_message_reply_2_count) as TextView?
        mReplyTextView3_1 = findViewById<View>(R.id.text_message_reply_3) as TextView?
        mReplyTextView3_2 = findViewById<View>(R.id.text_message_reply_3_count) as TextView?

        if (savedInstanceState != null) {
            val isVisible = savedInstanceState
                .getBoolean("reply_visible")
            if (isVisible) {
                mReplyHeadTextView?.setVisibility(View.VISIBLE)
                mReplyTextView1_1?.setText(
                    savedInstanceState
                        .getString("reply_text_applCount")
                )
                mReplyTextView1_1?.setVisibility(View.VISIBLE)
                mReplyTextView1_2?.setText(
                    savedInstanceState
                        .getString("reply_item_text_apple")
                )
                mReplyTextView1_2?.setVisibility(View.VISIBLE)
                mReplyTextView2_1?.setText(
                        savedInstanceState
                                .getString("reply_item_text_cheese")
                )
                mReplyTextView2_1?.setVisibility(View.VISIBLE)
                mReplyTextView2_2?.setText(
                        savedInstanceState
                                .getString("reply_cheeseCount")
                )
                mReplyTextView2_2?.setVisibility(View.VISIBLE)
                mReplyTextView3_1?.setText(
                        savedInstanceState
                                .getString("reply_cookiesCount")
                )
                mReplyTextView3_1?.setVisibility(View.VISIBLE)
                mReplyTextView3_2?.setText(
                        savedInstanceState
                                .getString("reply_item_text_cookies")
                )
                mReplyTextView3_2?.setVisibility(View.VISIBLE)
            }
        }
    }
    fun LaunchSecondActivity(view: View) {
        val intent = Intent(this, ItemsPage::class.java)
        startActivityForResult(intent, TEXT_REQUEST)
    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Test for the right intent reply.
        if (requestCode == TEXT_REQUEST) {
            // Test to make sure the intent reply result was good.
            if (resultCode == Activity.RESULT_OK) {
                val reply_applCount = data!!.getStringExtra(ItemsPage.EXTRA_REPLY_APPCOUNT)
                val reply_item_apple=data!!.getStringExtra(ItemsPage.EXTRA_REPLYITEM_APPLE)
                val reply_item_cheese=data.getStringExtra(ItemsPage.EXTRA_REPLYITEM_CHEESE)
                val reply_cheeseCount = data!!.getStringExtra(ItemsPage.EXTRA_REPLY_CHEESECOUNT)
                val reply_item_cookies=data.getStringExtra(ItemsPage.EXTRA_REPLYITEM_COOKIES)
                val reply_cookiesCount = data!!.getStringExtra(ItemsPage.EXTRA_REPLY_COOKIESCOUNT)
                // Make the reply head visible.
                mReplyHeadTextView!!.visibility = View.VISIBLE

                // Set the reply and make it visible.
                mReplyTextView1_2!!.text = reply_item_apple
                mReplyTextView1_2!!.visibility = View.VISIBLE
                mReplyTextView1_1!!.text = reply_applCount
                mReplyTextView1_1!!.visibility = View.VISIBLE
                mReplyTextView2_1!!.text = reply_item_cheese
                mReplyTextView2_1!!.visibility = View.VISIBLE
                mReplyTextView2_2!!.text = reply_cheeseCount
                mReplyTextView2_2!!.visibility = View.VISIBLE
                mReplyTextView3_1!!.text = reply_item_cookies
                mReplyTextView3_1!!.visibility = View.VISIBLE
                mReplyTextView3_2!!.text = reply_cookiesCount
                mReplyTextView3_2!!.visibility = View.VISIBLE
            }
        }
    }
    companion object {

        // Unique tag required for the intent extra
        const val EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE"

        // Unique tag for the intent reply
        const val TEXT_REQUEST = 1
    }
}