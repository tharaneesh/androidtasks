package com.example.tabsnavigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(fm: FragmentManager, behavior: Int, private val tabsNumber: Int) :
    FragmentPagerAdapter(fm, behavior) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> firstFragment()
            1 -> SecondFragment()
            else -> ThirdFragment()

        }
    }

    override fun getCount(): Int {
        return tabsNumber
    }
}