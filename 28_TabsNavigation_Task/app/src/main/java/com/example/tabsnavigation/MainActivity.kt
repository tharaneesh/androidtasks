package com.example.tabsnavigation

import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabItem
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener
{
var drawerLayout:DrawerLayout?=null
    var toggle:ActionBarDrawerToggle?=null
    var navigateView:NavigationView?=null
    var toolbar:Toolbar?=null
    var pager:ViewPager?=null
    var mTabLayout:TabLayout?=null
    var firstItem: TabItem?=null
    var secondItem: TabItem?=null
    var thirdItem: TabItem?=null
    var adapter:PagerAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
      setSupportActionBar(findViewById(R.id.toolbar))
        toolbar=findViewById(R.id.toolbar)
//        setSupportActionBar(toolbar)
        pager=findViewById(R.id.viewPager)
        mTabLayout=findViewById(R.id.tab_layout)
        firstItem=findViewById(R.id.first_item)
        secondItem=findViewById(R.id.second_item)
        thirdItem=findViewById(R.id.third_item)
        drawerLayout=findViewById(R.id.drawer)
        navigateView=findViewById(R.id.nav_view)
        navigateView!!.setNavigationItemSelectedListener(this)
        toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close)
        drawerLayout!!.addDrawerListener(toggle!!)
        toggle!!.setDrawerIndicatorEnabled(true)
        toggle!!.syncState()
        adapter = PagerAdapter(
            getSupportFragmentManager(),
            FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
            mTabLayout!!.getTabCount()
        )

      pager!!.setAdapter(adapter)


        mTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager!!.setCurrentItem(tab.getPosition());
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
       // pager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mTabLayout))


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawerLayout?.closeDrawer(GravityCompat.START)
       if(item.getItemId()==R.id.menuitem)
       {
           Toast.makeText(this,"Button Clicked",Toast.LENGTH_LONG).show()
       }
        if(item.getItemId()==R.id.item2)
            Toast.makeText(this,"Settings Clicked",Toast.LENGTH_LONG).show()
        if (item.getItemId()==R.id.item3)
            Toast.makeText(this,"Gallery Clicked",Toast.LENGTH_LONG).show()
        return false
    }
}

