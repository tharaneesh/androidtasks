package com.example.rv_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    var recylclerView_main : RecyclerView?=null
    var items: MutableList<listModel> = ArrayList()
    lateinit var mAdapter : MainAdapter
    var mTextView : TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recylclerView_main=findViewById(R.id.rv_id)
        initRecyclerView()
        addDataSet()
    }
    private fun addDataSet() {
        val data = DataSource.createDataSet()
        items.addAll(data)
       // println(items)
        mAdapter.submitList(data)

    }

    private fun initRecyclerView() {
        recylclerView_main?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingDecoration = ItemDecoration(20 )
            addItemDecoration(topSpacingDecoration)
            mAdapter= MainAdapter(items as ArrayList<listModel>)
            recylclerView_main!!.adapter=mAdapter

        }
    }
        fun add(count: Int) {
          mTextView
        }

}