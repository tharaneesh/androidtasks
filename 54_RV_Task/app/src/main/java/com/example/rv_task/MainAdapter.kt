package com.example.rv_task

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.content_button.view.*
import kotlinx.android.synthetic.main.content_text.view.*

class MainAdapter(data: ArrayList<listModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder> () {
    var items: MutableList<listModel> = ArrayList()
    lateinit var mVH : itemViewHolder
    init {
        this.items= data
    }
    companion object{
        var count =0
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       var vH : View=LayoutInflater.from(parent.context).inflate(R.layout.content_button, parent, false)
        if(viewType==0) {
            vH = LayoutInflater.from(parent.context).inflate(R.layout.content_text, parent, false)
            return itemViewHolder(vH)
        }
        return buttonViewHolder(vH)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is itemViewHolder ->
                holder.bind()
            is buttonViewHolder ->
                holder.bind()
        }
    }
    override fun getItemViewType(position: Int): Int {
        println(position)
        var viewType = 1
        if (position == 0) viewType = 0
        return viewType
    }
    fun submitList(item: List<listModel>){
        items= item as MutableList<listModel>
    }
    override fun getItemCount(): Int {
    return items.size
    }

     inner class buttonViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mButton = itemView.button
        fun bind() {
            mButton.setOnClickListener {
            count++
            mVH.add(count)
            }
        }
     }
      inner class itemViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mNumtext = itemView.textView

         fun add(count: Int) {
             mNumtext.setText(count.toString())
         }
          fun bind() {
          mVH = itemViewHolder(itemView)
          }
      }
    }


