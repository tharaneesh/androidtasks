package com.example.sports_recycler_activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sports_recycler_activity.model.SportsPost
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var sportsAdapter: SportsAdapter
    private var mRecyclerView : RecyclerView?=null

    var items: MutableList<SportsPost> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRecyclerView = findViewById(R.id.recycler_view);
        initRecyclerView()
        addDataSet()
        val helper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
                    ItemTouchHelper.DOWN or ItemTouchHelper.UP,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                val from = viewHolder.adapterPosition
                val to = target.adapterPosition
                println(items)
                println(to)

                Collections.swap(items, from, to)
                sportsAdapter.notifyItemMoved(from, to)
                return true
            }


            override fun onSwiped(
                viewHolder: RecyclerView.ViewHolder,
                direction: Int
            ) {
                val from = viewHolder.adapterPosition
                println(from)
                val position = viewHolder.adapterPosition
                sportsAdapter.deleteItem(position)
                sportsAdapter.notifyDataSetChanged()
            }
        })
        helper.attachToRecyclerView(recycler_view);
      //  sportsAdapter.notifyDataSetChanged();
    }
    private fun addDataSet()
    {
        val data = DataSource.createDataSet()
        items.addAll(data)
        sportsAdapter.submitList(data)

    }
    private fun initRecyclerView(){
        recycler_view.apply{
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingDecoration = ItemDecoration(30 )
            addItemDecoration(topSpacingDecoration)
            sportsAdapter= SportsAdapter(this@MainActivity,items as ArrayList<SportsPost>)
            adapter=sportsAdapter
        }
    }

    fun resetSports(view: View) {
        initRecyclerView()
        addDataSet()
    }
}