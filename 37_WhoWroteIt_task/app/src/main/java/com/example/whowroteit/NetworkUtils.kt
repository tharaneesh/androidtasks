package com.example.whowroteit

import android.net.Uri
import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class NetworkUtils {
    private val LOG_TAG = NetworkUtils::class.java.simpleName

    private val BOOK_BASE_URL = "https://www.googleapis.com/books/v1/volumes?"

    private val QUERY_PARAM = "q"

    private val MAX_RESULT = "maxResults"

    private val PRINT_TYPE = "printType"

    companion object {
        fun getBookInfo(networkUtils: NetworkUtils, queryString: String?): String? {

            var urlConnection: HttpURLConnection? = null
            var reader: BufferedReader? = null
            var bookJSONString: String? = null
            try {
                val builtURI = Uri.parse(networkUtils.BOOK_BASE_URL).buildUpon()
                    .appendQueryParameter(networkUtils.QUERY_PARAM, queryString)
                    .appendQueryParameter(networkUtils.MAX_RESULT, "10")
                    .appendQueryParameter(networkUtils.PRINT_TYPE, "books")
                    .build()

                val requestURL = URL(builtURI.toString())

                urlConnection = requestURL.openConnection() as HttpURLConnection
                urlConnection.requestMethod = "GET"
                urlConnection!!.connect()

                val inputStream = urlConnection.inputStream

                reader = BufferedReader(InputStreamReader(inputStream))

                val builder = StringBuilder()
                var line: String?
                while (reader.readLine().also { line = it } != null) {
                    builder.append(line)

                    builder.append("\n")
                }
                if (builder.length == 0) {
                    return null
                }
                bookJSONString = builder.toString()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                urlConnection?.disconnect()
                if (reader != null) {
                    try {
                        reader.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }

            Log.d(networkUtils.LOG_TAG, bookJSONString!!)
            return bookJSONString
        }
    }

}