package com.example.whowroteit

import android.os.AsyncTask
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject


class FetchBook(
    var mTitleText: TextView?,
    var mAuthorText: TextView?
) : AsyncTask<String?, Void?, String?>() {

    override fun doInBackground(vararg strings: String?): String? {
        return NetworkUtils.getBookInfo(NetworkUtils(),strings[0])
    }
    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        try {
            val jsonObject = JSONObject(result)
            val itemsArray: JSONArray = jsonObject.getJSONArray("items")

            var i = 0
            var title: String? = null
            var authors: String? = null

            while (i < itemsArray.length() &&
                authors == null && title == null
            ) {
                val book = itemsArray.getJSONObject(i)
                val volumeInfo = book.getJSONObject("volumeInfo")

                try {
                    title = volumeInfo.getString("title")
                    authors = volumeInfo.getString("authors")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                i++
            }

            if (title != null && authors != null) {
                mTitleText!!.setText(title)
                mAuthorText!!.setText(authors)
            } else {
                mTitleText!!.setText("No results")
                mAuthorText!!.setText("")
            }
        } catch (e: Exception) {
            mTitleText!!.setText("No results")
            mAuthorText!!.setText("")
        }

    }
    init {
        this.mAuthorText=mAuthorText
        this.mTitleText=mTitleText
    }
}
