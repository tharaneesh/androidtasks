package com.example.whowroteit

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName
    private var mBookInput: EditText? = null
    private var mTitleText: TextView? = null
    private var mAuthorText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mBookInput= findViewById(R.id.inputText)
        mTitleText= findViewById(R.id.bookTitle)
        mAuthorText= findViewById(R.id.bookAuthor)
    }

    fun buttonClick(view: View) {
        val queryString = mBookInput!!.text.toString()
        val inputManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }


        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo: NetworkInfo? = null
        if (connMgr != null) {
            networkInfo = connMgr.activeNetworkInfo
        }
    println(networkInfo)
        if (networkInfo != null && networkInfo.isConnected
            && queryString.length != 0
        ) {
            println(networkInfo)

            FetchBook(mTitleText, mAuthorText).execute(queryString)
            mAuthorText!!.text = ""
            mTitleText!!.setText("Loading...")
        } else {
            if (queryString.length == 0) {
                mAuthorText!!.text = ""
                mTitleText!!.setText("No search item")
            } else {
                mAuthorText!!.text = ""
                mTitleText!!.setText("No network")
            }
        }
        Log.i(TAG, "searchbooks: "+queryString);
    }
}