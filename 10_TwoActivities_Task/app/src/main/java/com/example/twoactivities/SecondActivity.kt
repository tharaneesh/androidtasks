package com.example.twoactivities


import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val intent = intent
        val message = intent.getStringExtra(MainActivity().EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.text_message)
        textView.setText(message);
        val button=findViewById<Button>(R.id.button_back)
        button.setOnClickListener {
            finish()
        }

    }

    fun returnReply(view: View) {}
}