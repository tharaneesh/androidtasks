package com.example.twoactivities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private var mMessageEditText: EditText? = null
    val EXTRA_MESSAGE = "com.example.android.twoactivities.extra.MESSAGE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
           mMessageEditText = findViewById(R.id.editText_second)


       }

    fun LaunchSecondActivity(view: View) {
        val comment: Toast = Toast.makeText(this, R.string.msg,
            Toast.LENGTH_SHORT)
        comment.show()
        val intent = Intent(this, SecondActivity::class.java)
        val message: String = mMessageEditText?.getText().toString()
        intent.putExtra(EXTRA_MESSAGE, message)


        startActivity(intent);

    }
}
