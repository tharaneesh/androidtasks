package com.example.count_onsavedinstance_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var mCount=0
    private var button_inc : TextView? = null
    private var count_text : TextView? = null
    private var button_dec : TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         button_inc=findViewById(R.id.button_increment) as TextView?
        count_text =findViewById(R.id.text_view_count) as TextView?
        button_dec=findViewById(R.id.button_decrement) as TextView?
        button_inc?.setOnClickListener {
            mCount++
            count_text!!.text=mCount.toString()
        }
        button_dec?.setOnClickListener {
            mCount--
            count_text!!.text=mCount.toString()
        }

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val num=mCount
        outState.putInt("count", mCount)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val userInt=savedInstanceState.getInt("count",0)
        mCount=userInt
        count_text!!.text = mCount.toString()
    }
}