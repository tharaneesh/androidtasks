package com.example.articles.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.articles.R
import com.example.articles.models.tI_list
import kotlinx.android.synthetic.main.sports.view.*

class SportsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    var items: MutableList<tI_list> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return articleViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.sports, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is articleViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
    fun submitList(article_list: List<tI_list>){
        items= article_list as MutableList<tI_list>
    }
    inner class articleViewHolder constructor(itemView: View):
        RecyclerView.ViewHolder(itemView) {


        val Image = itemView.secondary_sports_image
        val Title = itemView.secondary_sports_title
        fun bind(source: tI_list){
            Title.setText(source.title)
            val reqOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
            Glide.with(itemView.context)
                .applyDefaultRequestOptions(reqOptions)
                .load(source.image).into(Image)
        }
    }
}