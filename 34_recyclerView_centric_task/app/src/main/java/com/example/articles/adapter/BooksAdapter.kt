package com.example.articles.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.articles.R
import com.example.articles.models.text_list
import kotlinx.android.synthetic.main.books.view.*

class BooksAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var items: MutableList<text_list> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return articleViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.books, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is articleViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
    fun submitList(article_list: List<text_list>){
        items= article_list as MutableList<text_list>
    }
    inner class articleViewHolder constructor(itemView: View):
            RecyclerView.ViewHolder(itemView) {



        val Title = itemView.book_text

        fun bind(source: text_list){
            Title.setText(source.title)
        }
    }
}