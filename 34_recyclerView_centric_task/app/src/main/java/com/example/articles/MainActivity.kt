package com.example.articles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.articles.DataSource.PrimaryDataSource
import com.example.articles.DataSource.SecondaryDataSource
import com.example.articles.adapter.*
import com.example.articles.models.tI_list
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var marticleAdapter: PrimaryAdapterSports
    private lateinit var marticleAdapterfruit: PrimaryAdapterFruit
    private lateinit var marticleAdapterbook: PrimaryAdapterBook
    private lateinit var marticleAdapter_sec: SportsAdapter
    private lateinit var marticleAdapter_fruits_sec: FruitsAdapter
    private lateinit var marticleAdapter_books_sec: BooksAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()

    }

    private fun addDataSet_Books_sec() {
        val data = SecondaryDataSource.createDataSetBooks()
        marticleAdapter_books_sec.submitList(data)
    }

    private fun addDataSet_Books() {
        val data = PrimaryDataSource.createDataBooks()
        marticleAdapterbook.submitList(data)
    }

    private fun addDataSet_fruits_sec() {
        val data = SecondaryDataSource.createDataSetFruits()
        marticleAdapter_fruits_sec.submitList(data)
    }

    private fun addDataSet_Fruits() {
        val data = PrimaryDataSource.createDataFruits()
        marticleAdapterfruit.submitList(data)

    }

    private fun addDataSet_sports_sec() {
        val data = SecondaryDataSource.createDataSet()
        marticleAdapter_sec.submitList(data)
    }

    private fun addDataSet_sports() {
        val data = PrimaryDataSource.createDataSports()
        marticleAdapter.submitList(data)

    }

    private fun initRecyclerView() {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
           val topSpacingDecoration = ItemDecoration(30)
            addItemDecoration(topSpacingDecoration)
            marticleAdapter = PrimaryAdapterSports()
            addDataSet_sports()
            marticleAdapter_sec =  SportsAdapter()
            addDataSet_sports_sec()
            marticleAdapterfruit = PrimaryAdapterFruit()
            addDataSet_Fruits()
            marticleAdapter_books_sec = BooksAdapter()
            addDataSet_Books_sec()
            marticleAdapter_fruits_sec = FruitsAdapter()
            addDataSet_fruits_sec()
            marticleAdapterbook = PrimaryAdapterBook()
            addDataSet_Books()

            val concatAdapter = ConcatAdapter(marticleAdapter,marticleAdapter_sec,marticleAdapterfruit,GridConcatAdapter(this@MainActivity,marticleAdapter_fruits_sec,2),marticleAdapterbook,marticleAdapter_books_sec)
            recycler_view.adapter=concatAdapter

        }
    }
}