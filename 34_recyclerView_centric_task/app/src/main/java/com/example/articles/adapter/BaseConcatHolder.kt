package com.example.articles.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseConcatHolder<Dum>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(adapter: Dum)
}