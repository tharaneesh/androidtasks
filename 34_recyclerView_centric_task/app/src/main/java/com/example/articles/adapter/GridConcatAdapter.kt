package com.example.articles.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.articles.R
import kotlinx.android.synthetic.main.concat_row.view.*

class GridConcatAdapter(private val context: Context, private val fruitsAdapter: FruitsAdapter, private val spanCount:Int) :
RecyclerView.Adapter<BaseConcatHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseConcatHolder<*> {
        val view = LayoutInflater.from(context).inflate(R.layout.concat_row,parent,false)
        view.rv_grid_concat.layoutManager = GridLayoutManager(context, spanCount)
        return ConcatViewHolder(view)
    }
    override fun getItemCount(): Int  = 1

    override fun onBindViewHolder(holder: BaseConcatHolder<*>, position: Int) {
        when(holder){
            is ConcatViewHolder -> holder.bind(fruitsAdapter)
        }
    }

    inner class ConcatViewHolder(itemView: View): BaseConcatHolder<FruitsAdapter>(itemView){
        override fun bind(adapter: FruitsAdapter) {
            itemView.rv_grid_concat.adapter = adapter
        }
    }
}