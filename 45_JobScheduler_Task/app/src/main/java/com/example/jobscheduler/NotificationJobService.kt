package com.example.jobscheduler

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat


class NotificationJobService : JobService() {
    lateinit var mNotificationManager: NotificationManager
    val PRIMARY_CHANNEL_ID = "primary_notification_channel"
    override fun onStartJob(params: JobParameters?): Boolean {
   createNotificationChannel()
//        val contentPendingIntent = PendingIntent.getActivity(
//            this, 0, Intent(
//                this,
//                MainActivity::class.java
//            ), PendingIntent.FLAG_UPDATE_CURRENT
//        )

        val builder = NotificationCompat.Builder(this,"primary_notification_channel")
        builder.setSmallIcon(R.drawable.ic_job_running)
            .setContentTitle("Job Service")
            .setContentText("You job ran to completion")
            .setAutoCancel(true)

        mNotificationManager.notify(0,builder.build())
        return false
    }

    override fun onStopJob(params: JobParameters?): Boolean {
    return true
    }

    fun createNotificationChannel() {

         mNotificationManager= getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.O
        ) {

            val notificationChannel = NotificationChannel(
                PRIMARY_CHANNEL_ID,
                "Job Service Notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.description = "Notification from Job Service"
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
    }

}