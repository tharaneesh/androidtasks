package com.example.jobscheduler

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private var mScheduler: JobScheduler? = null
    private val JOB_ID = 0
    private var mDeviceIdleSwitch: Switch? = null
    private var mDeviceChargingSwitch: Switch? = null
    private var mSeekBar: SeekBar? = null
    private var seekBarProgress : TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDeviceIdleSwitch = findViewById(R.id.idleSwitch)
        mDeviceChargingSwitch = findViewById(R.id.chargingSwitch)
        mSeekBar = findViewById(R.id.seekBar)
        seekBarProgress = findViewById(R.id.seekBarProgress)

        mSeekBar!!.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                println(i)
                if (i > 0){
                    val k = i
                    seekBarProgress!!.setText("Set " + k+"%");
                }else {
                    seekBarProgress!!.setText("Not Set");
                }

            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })


    }

    fun jobSchedule(view: View) {
        val networkOptions = findViewById<RadioGroup>(R.id.networkOptions)
        mScheduler = getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler
        val selectedNetworkID: Int = networkOptions.checkedRadioButtonId
        val seekBarInteger = mSeekBar!!.progress
        val seekBarSet = seekBarInteger > 0

        // println(selectedNetworkID)
        var selectedNetworkOption = JobInfo.NETWORK_TYPE_NONE
        when (selectedNetworkID) {
            R.id.radioButton_none -> selectedNetworkOption = JobInfo.NETWORK_TYPE_NONE
            R.id.radioButton_any -> selectedNetworkOption = JobInfo.NETWORK_TYPE_ANY
            R.id.radioButton_Wifi-> selectedNetworkOption = JobInfo.NETWORK_TYPE_UNMETERED
        }
        println(selectedNetworkOption)
        val serviceName = ComponentName(
            packageName,
            NotificationJobService::class.java.name
        )
        val builder = JobInfo.Builder(JOB_ID, serviceName)
            .setRequiredNetworkType(selectedNetworkOption)
            .setRequiresDeviceIdle(mDeviceIdleSwitch!!.isChecked())
            .setRequiresCharging(mDeviceChargingSwitch!!.isChecked())

        if (seekBarSet) {
            builder.setOverrideDeadline((seekBarInteger * 1000).toLong());
        }

        val constraintSet = (selectedNetworkOption != JobInfo.NETWORK_TYPE_NONE)
                || mDeviceChargingSwitch!!.isChecked() || mDeviceIdleSwitch!!.isChecked()
                || seekBarSet

        println(constraintSet)
        if(constraintSet) {
            val myJobInfo = builder.build()
            mScheduler!!.schedule(myJobInfo)
            Toast.makeText(
                this, "Job Scheduled, job will run when " +
                        "the constraints are met.", Toast.LENGTH_SHORT
            ).show();
        }
        else
        {
            Toast.makeText(this, "Please set at least one constraint",
                Toast.LENGTH_SHORT).show();

        }

    }
    fun cancelJob(view: View) {
        if (mScheduler!=null){
            mScheduler!!.cancelAll();
            mScheduler = null;
            Toast.makeText(this, "Jobs cancelled", Toast.LENGTH_SHORT).show();
        }

    }
}