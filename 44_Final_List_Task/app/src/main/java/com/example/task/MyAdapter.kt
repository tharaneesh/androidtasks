package com.example.task

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.contents.view.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MyAdapter(context: Context, products: Array<models>, count: Int):
    RecyclerView.Adapter<RecyclerView.ViewHolder> () {

    var products_comment:List<comments_model>?=null
    var items: Array<models>
    var mContext: Context? = null
    var count: Int
    var pos = 0
    init {
        mContext = context
        items = products
        this.count = count
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return articleViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.contents, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is articleViewHolder ->
                holder.bind(items.get(position))
        }
    }


    inner class articleViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val user_id = itemView.user_ID_text
        val id_no = itemView.ID_text
        val Title = itemView.title_text
        val Body = itemView.body_text

        fun bind(source: models) {
            Title.setText(source.title)
            Body.setText(source.body)
            user_id.setText("UserID =  " + source.userId)
            id_no.setText(("ID = " + source.id))
        }

        init {

            itemView.setOnClickListener {
                 pos = adapterPosition
                var parameter = 1

                if (pos > 4) {
                    val Temp = pos + 1
                    if (Temp % 5 != 0)
                        parameter = Temp / 5 + 1
                    else
                        parameter = Temp / 5
                }
                val Post_ID = parameter
                getCurrentData(Post_ID,itemView)

           }

        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    override fun getItemCount(): Int {
        return items.size

    }

    private fun getCurrentData(value: Int, itemView: View): List<comments_model>? {

        val postID =value
        var itemview : View
        itemview = itemView



        val Base_url = "https://jsonplaceholder.typicode.com/posts/"+postID+"/"
        val api= Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Base_url)
            .build()
        val service = api.create(ApiInterface::class.java)

        val res = service.getList()
        res.enqueue(object  : retrofit2.Callback<List<comments_model>>{

            override fun onResponse(
                call: retrofit2.Call<List<comments_model>>,
                response: retrofit2.Response<List<comments_model>>
            ) {
                products_comment = response.body()
                settingValues(itemview)
            }

            override fun onFailure(call: retrofit2.Call<List<comments_model>>, t: Throwable) {
                println("Failure")
            }

        })
        return products_comment
    }

    private fun settingValues(itemView: View) {
        val content: models
                var comments: comments_model
                content = items[pos]
                if (count == 1) {
                    val intent = Intent(itemView.context, CommentsActivity::class.java)

                        val check = pos%5
                        comments = products_comment!![check]
                        intent.putExtra("actionBar_title",content.title)
                        intent.putExtra("name", comments.name)
                        intent.putExtra("user_id", comments.postId)
                        intent.putExtra("ID",comments.id)
                        intent.putExtra("email", comments.email)
                        intent.putExtra("body", comments.body)
                    itemView.context.startActivity(intent)
                }

                if(count==2) {

                        val layout = itemView.layout
                        val layout_comments = itemView.layout_comments
                        val check = pos%5
                        comments= products_comment!![check]
                        if(layout_comments.isVisible) {
                            layout_comments.setVisibility(View.GONE)
                            layout.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.cardview_shadow_start_color))
                            layout_comments.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.design_default_color_background))
                        }
                       else {
                            layout_comments.setVisibility(View.VISIBLE)
                            layout.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.design_default_color_background))
                            layout_comments.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.purple_200))
                        }

                       val title = itemView.title_text_com
                       val email = itemView.email_text
                       val body = itemView.body_text_com
                       val u_id = itemView.id_text_com
                       val post_id = itemView.user_id_text_com

                       title.text = comments.name
                       email.text = comments.email
                       body.text =  comments.body
                       u_id.text =  "ID:" + comments.id
                       post_id.text = "Post ID:" + comments.postId

                   }

    }


}