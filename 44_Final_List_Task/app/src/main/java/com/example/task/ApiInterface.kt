package com.example.task

import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("comments")
    fun getList() : Call<List<comments_model>>
}