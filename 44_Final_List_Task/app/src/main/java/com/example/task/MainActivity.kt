package com.example.task

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
class MainActivity : AppCompatActivity() {
    var recylclerView_main : RecyclerView?=null
    lateinit var mAdapter : MyAdapter
    lateinit var products: Array<models>
    var count =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         count = resources.getInteger(R.integer.count)
         recylclerView_main = findViewById(R.id.rv_id)
         fetchJson()

    }


    private fun initRecyclerView() {
            recylclerView_main?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingDecoration = ItemDecoration(20 )
            addItemDecoration(topSpacingDecoration)
        }
    }



    private fun fetchJson() {

        val url ="https://jsonplaceholder.typicode.com/posts"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback{


            override fun onFailure(call: Call, e: IOException) {
                println("Failed")
            }
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                products = Gson().fromJson(body,Array<models>::class.java)
                runOnUiThread {
                    initRecyclerView()
                    mAdapter = MyAdapter(this@MainActivity,products,count)
                    recylclerView_main!!.adapter = mAdapter
                }
            }
        })
    }


}
