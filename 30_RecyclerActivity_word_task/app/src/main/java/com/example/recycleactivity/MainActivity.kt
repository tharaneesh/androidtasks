package com.example.recycleactivity


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*



class MainActivity : AppCompatActivity() {
    private val mWordList: LinkedList<String> = LinkedList()
    private lateinit var  mRecyclerView : RecyclerView
    private lateinit var mAdapter: WordListAdapter
    var fab_count = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setImageResource(R.drawable.ic_add_for_fab)
        fab.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                fab_count++
                val wordListSize = mWordList.size
                mWordList.addLast("+ Word $wordListSize")
                mRecyclerView.adapter!!.notifyItemInserted(wordListSize)
                mRecyclerView.smoothScrollToPosition(wordListSize)
            }
        })

        for (i in 0..19) {
            mWordList.addLast("Word $i")
        }

        mRecyclerView = findViewById(R.id.recycler_view)
//        mAdapter = WordListAdapter(this, mWordList)
//        mRecyclerView.adapter = mAdapter
//        mRecyclerView.layoutManager = LinearLayoutManager(this)
       initRecyclerView()

    }
   private fun initRecyclerView(){
        recycler_view.apply{
            layoutManager = LinearLayoutManager(this@MainActivity)
            mAdapter = WordListAdapter(this@MainActivity,mWordList)
            adapter = mAdapter
        }

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
private fun reset()
{
    for(i in 0..fab_count)
    {
        mWordList.removeLast()
    }
    fab_count=-1
    for(i in 0..mWordList.size-1)
    {
        if (mWordList[i].contains("Clicked!"))
        {
            mWordList[i]="Word $i"
        }
    }
    initRecyclerView()
}
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.getItemId()==R.id.action_settings)
        {
            reset()
        }
return false
    }
}