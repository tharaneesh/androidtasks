package com.example.implicit_intent_task

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat


class MainActivity : AppCompatActivity() {
    private var mWebsiteEditText: EditText? = null
    private var mLocationEditText: EditText? = null
    private var mShareTextEditText: EditText? = null
    private var mimageview: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mWebsiteEditText=findViewById(R.id.Website_text)
        mLocationEditText = findViewById(R.id.location_text);
        mShareTextEditText = findViewById(R.id.Share_text);
        mimageview = findViewById(R.id.cameraview)
    }

    fun openWebsite(view: View) {
        val url = mWebsiteEditText!!.text.toString()
        val webpage: Uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
             startActivity(intent)
    }
    fun openLocation(view: View) {
        val loc = mLocationEditText!!.text.toString()
        val addressUri = Uri.parse("geo:0,0?q=$loc")
        val intent = Intent(Intent.ACTION_VIEW, addressUri)
            startActivity(intent)
    }
    fun shareText(view: View) {
        val txt = mShareTextEditText!!.text.toString()
        val mimeType = "text/plain"
        ShareCompat.IntentBuilder
            .from(this)
            .setType(mimeType)
            .setChooserTitle("share_text_with: ")
            .setText(txt)
            .startChooser();


    }

    fun Take_Picture(view: View) {
        val intent =Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivity(intent)
    }
}