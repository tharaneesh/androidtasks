package com.example.scrollview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun Show_comment(view: View) {
        val comment: Toast = Toast.makeText(this, R.string.msg,
            Toast.LENGTH_SHORT)
        comment.show()
    }
}