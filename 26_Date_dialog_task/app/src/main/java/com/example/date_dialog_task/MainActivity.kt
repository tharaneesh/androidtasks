package com.example.date_dialog_task

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.*
import java.util.*


class MainActivity : AppCompatActivity(),DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {
    var mdata_button: Button?=null
    var mdata_text:TextView?=null
    var result: SpannableStringBuilder = SpannableStringBuilder("")
    var day=0
    var month=0
    var year=0
    var hour=0
    var minute=0

    var savedDay=0
    var savedMonth=0
    var savedYear=0
    var savedHour=0
    var savedMinute=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mdata_button=findViewById(R.id.date_button)
        mdata_text=findViewById(R.id.textView2)
        pickDate()

    }
    private fun getDateTimeCalendar()
    {
        val c = Calendar.getInstance()
         year = c.get(Calendar.YEAR)
         month = c.get(Calendar.MONTH)
         day = c.get(Calendar.DAY_OF_MONTH)
        hour = c.get(Calendar.YEAR)
        minute = c.get(Calendar.MINUTE)

    }
    private fun pickDate()
    {
        mdata_button ?.setOnClickListener {
            getDateTimeCalendar()
            DatePickerDialog(this,this,year,month,day).show()}
    }
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay=dayOfMonth
        savedMonth=month
        savedYear=year

        getDateTimeCalendar()
        TimePickerDialog(this,this,hour,minute,true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour=hourOfDay
        savedMinute=minute

        mdata_text!!.text="$savedDay-$savedMonth-$savedYear\n$savedHour hr $savedMinute min"
        result.append(mdata_text!!.text.toString())
        Toast.makeText(this,result.toString(),Toast.LENGTH_SHORT).show()

    }
}