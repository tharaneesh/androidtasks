package com.example .myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var mCount = 0
    private var mShowCount: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mShowCount = findViewById<View>(R.id.textView2) as TextView

        var textView =findViewById<TextView>(R.id.button_Zer0)
        textView.setTextColor(Color.parseColor("#FF0000"))


    }


    fun showToast(view: View?) {
        val toast: Toast = Toast.makeText(this, R.string.msg,
            Toast.LENGTH_SHORT)
        toast.show()
    }


    fun countUp(view: View?) {
        mCount++
        if (mShowCount != null)
        {
            mShowCount!!.text = Integer.toString(mCount)
            val buttons: Button =findViewById(R.id.button_Zer0)
            buttons.setBackgroundColor(Color.YELLOW)
            val button: Button =findViewById(R.id.button_toast)
            button.setBackgroundColor(Color.DKGRAY)


        }

    }

    fun colorChange(view: View?) {

     //   mShowColor.setTextColor(Color.parseColor(#FF0000))
    }
}