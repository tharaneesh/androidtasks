package com.example.unittest

object Regsitration_Util {

    private val existingusers = listOf("peter","carl")
    fun validateRegistrationInput(
        username: String,
        password: String,
        confirmedPassword: String
    ):Boolean{
        if(username.isEmpty()||password.isEmpty()){
            return false
        }
        if(username in existingusers)
        {
            return false
        }
        if(password!=confirmedPassword)
        {
            return false
        }
        if(password.count{it.isDigit()}<2)
        {
            return false
        }
        return true
    }
}