package com.example.unittest

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class Regsitration_UtilTest{

  @Test
   fun empty_username_returns_false (){
  val result = Regsitration_Util.validateRegistrationInput(
      "",
      "123",
      "123"

  )
   assertThat(result).isFalse()

    }
    @Test
    fun valid_username_correct_password_returns_true (){
        val result = Regsitration_Util.validateRegistrationInput(
            "tharan",
            "123",
            "123"

        )
        assertThat(result).isTrue()
       // assertThat("hello").isEqualTo("hello")
    }
    @Test
    fun username_exists_returns_false (){
        val result = Regsitration_Util.validateRegistrationInput(
            "carl",
            "123",
            "123"

        )
        assertThat(result).isFalse()

    }
    @Test
    fun incorrect_password_returns_false (){
        val result = Regsitration_Util.validateRegistrationInput(
            "tharan",
            "123",
            "12345"

        )
        assertThat(result).isFalse()
    }
    @Test
    fun empty_passord_returns_false (){
        val result = Regsitration_Util.validateRegistrationInput(
            "tharan",
            "",
            ""

        )
        assertThat(result).isFalse()
    }
    @Test
    fun lessthan_2digits_returns_false (){
        val result = Regsitration_Util.validateRegistrationInput(
            "tharan",
            "1abcd",
            "1abcd"

        )
        assertThat(result).isFalse()
    }
}