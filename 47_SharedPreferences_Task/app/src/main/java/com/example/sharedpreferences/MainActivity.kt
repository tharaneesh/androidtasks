package com.example.sharedpreferences

import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {
    private var mCount = 0
    private var mColor = 0
    private var mShowCountTextView: TextView? = null
    private val COUNT_KEY = "count"
    private val COLOR_KEY = "color"

    private var mPreferences: SharedPreferences? = null
    private val sharedPrefFile = "com.example.android.sharedprefs"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mShowCountTextView = findViewById(R.id.count_textview)
        mColor = ContextCompat.getColor(
            this,
            R.color.default_background
        )
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mCount = mPreferences!!.getInt(COUNT_KEY, 0)

        mShowCountTextView!!.setText(String.format("%s", mCount));
        mColor = mPreferences!!.getInt(COLOR_KEY, mColor);
        mShowCountTextView?.setBackgroundColor(mColor);
        
//        if (savedInstanceState != null) {
//            mCount = savedInstanceState.getInt(COUNT_KEY)
//            if (mCount != 0) {
//                mShowCountTextView!!.setText(String.format("%s", mCount))
//            }
//
//            mColor = savedInstanceState.getInt(COLOR_KEY)
//            mShowCountTextView!!.setBackgroundColor(mColor)
//        }
    }

    fun changeBackground(view: View) {
        val color = (view.getBackground() as ColorDrawable).color
        mShowCountTextView!!.setBackgroundColor(color)
        mColor = color
    }

    fun countUp(view: View?) {
        mCount++
        mShowCountTextView!!.text = String.format("%s", mCount)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(COUNT_KEY, mCount)
        outState.putInt(COLOR_KEY, mColor)
    }


    fun reset(view: View?) {
        mCount = 0
        mShowCountTextView!!.text = String.format("%s", mCount)

        mColor = ContextCompat.getColor(
            this,
            R.color.default_background
        )
        mShowCountTextView!!.setBackgroundColor(mColor)
        val preferencesEditor = mPreferences!!.edit()
        preferencesEditor.clear()
        preferencesEditor.apply()

    }
    override fun onPause() {
        super.onPause()
        val preferencesEditor = mPreferences!!.edit()
        preferencesEditor.putInt(COUNT_KEY, mCount);
        preferencesEditor.putInt(COLOR_KEY, mColor);
        preferencesEditor.apply();



    }
}