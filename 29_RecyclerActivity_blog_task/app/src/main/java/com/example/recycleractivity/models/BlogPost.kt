package com.example.recycleractivity.models

data class BlogPost(
    var title: String,
    var image:String,
    val username: String
){

}