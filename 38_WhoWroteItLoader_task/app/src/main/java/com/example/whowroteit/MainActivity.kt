package com.example.whowroteit

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import org.json.JSONArray
import org.json.JSONObject


class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<String> {
    private val TAG = MainActivity::class.java.simpleName
    private var mBookInput: EditText? = null
    private var mTitleText: TextView? = null
    private var mAuthorText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mBookInput= findViewById(R.id.inputText)
        mTitleText= findViewById(R.id.bookTitle)
        mAuthorText= findViewById(R.id.bookAuthor)
           // getSupportLoaderManager().initLoader(0,null,this);
       getSupportLoaderManager().initLoader(0,null,this);

    }

    fun buttonClick(view: View) {
        val queryString = mBookInput!!.text.toString()
        val inputManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }


        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo: NetworkInfo? = null
        if (connMgr != null) {
            networkInfo = connMgr.activeNetworkInfo
        }
    println(networkInfo)
        if (networkInfo != null && networkInfo.isConnected
            && queryString.length != 0
        ) {
            val queryBundle = Bundle()
            queryBundle.putString("queryString", queryString)
            supportLoaderManager.restartLoader(0, queryBundle, this)
            mAuthorText!!.text = ""
            mTitleText!!.setText("Loading...")
        } else {
            if (queryString.length == 0) {
                mAuthorText!!.text = ""
                mTitleText!!.setText("No search item")
            } else {
                mAuthorText!!.text = ""
                mTitleText!!.setText("No network")
            }
        }
        Log.i(TAG, "searchbooks: "+queryString);
    }

    override fun onCreateLoader(id: Int, args: Bundle?): BookLoader {
        var queryString: String? = ""

        if (args != null) {
            queryString = args.getString("queryString")
        }

        return BookLoader(this, queryString!!)
    }

    override fun onLoadFinished(loader: Loader<String>, data: String?) {
        try {
            val jsonObject = JSONObject(data)
            val itemsArray: JSONArray = jsonObject.getJSONArray("items")

            var i = 0
            var title: String? = null
            var authors: String? = null

            while (i < itemsArray.length() &&
                authors == null && title == null
            ) {
                val book = itemsArray.getJSONObject(i)
                val volumeInfo = book.getJSONObject("volumeInfo")

                try {
                    title = volumeInfo.getString("title")
                    authors = volumeInfo.getString("authors")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                i++
            }

            if (title != null && authors != null) {
                mTitleText!!.setText(title)
                mAuthorText!!.setText(authors)
            } else {
                mTitleText!!.setText("No results")
                mAuthorText!!.setText("")
            }
        } catch (e: Exception) {
            mTitleText!!.setText("No results")
            mAuthorText!!.setText("")
        }
    }

    override fun onLoaderReset(loader: Loader<String>) {
    }
}