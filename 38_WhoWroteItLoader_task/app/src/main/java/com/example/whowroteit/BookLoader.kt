package com.example.whowroteit

import android.content.Context
import androidx.annotation.Nullable
import androidx.loader.content.AsyncTaskLoader


class BookLoader internal constructor(context: Context?, private var mQueryString: String) :
    AsyncTaskLoader<String?>(context!!) {
     override fun onStartLoading() {
        super.onStartLoading()
        forceLoad()
    }

    @Nullable
    override fun loadInBackground(): String? {
        return NetworkUtils.getBookInfo(NetworkUtils(),mQueryString)
    }
    init {
        this.mQueryString= mQueryString
    }
}