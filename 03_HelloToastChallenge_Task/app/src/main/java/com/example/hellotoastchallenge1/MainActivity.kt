package com.example.hellotoastchallenge1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var mCount = 0
    private var mShowCount: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mShowCount = findViewById<View>(R.id.Show_count) as TextView
    }

    fun show_value(view: View) {
        val toast: Toast = Toast.makeText(this, R.string.msg,
            Toast.LENGTH_LONG)
        toast.show()
    }
    fun show_text(view: View){
            mCount++
            if (mShowCount != null) mShowCount!!.text = Integer.toString(mCount)

    }
}