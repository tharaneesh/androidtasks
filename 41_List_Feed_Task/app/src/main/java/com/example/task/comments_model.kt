package com.example.task

import com.google.gson.annotations.SerializedName

data class comments_model(
    @SerializedName("postId")
    var postId:String?=null,
    @SerializedName("id")
    var id: String?=null,
    @SerializedName("name")
    var name: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("body")
    var body : String
){
}