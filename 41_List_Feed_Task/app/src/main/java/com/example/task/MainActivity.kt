package com.example.task

import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class MainActivity : AppCompatActivity() {
    var recylclerView_main : RecyclerView?=null
    lateinit var mAdapter : MyAdapter
    var products_comments : Array<comments_model>?=null
    lateinit var products: Array<models>
    var count =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         count = resources.getInteger(R.integer.count)
         recylclerView_main = findViewById(R.id.rv_id)
         fetchJson()
    }

    private fun initRecyclerView() {
            recylclerView_main?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingDecoration = ItemDecoration(20 )
            addItemDecoration(topSpacingDecoration)
        }
    }


    private fun fetchComments() {

         val url_comments ="https://jsonplaceholder.typicode.com/posts/1/comments/"
         val request_comments = Request.Builder().url(url_comments).build()
        println("request_comments = "+ request_comments)
         val client_comments = OkHttpClient()

         client_comments.newCall(request_comments).enqueue(object : Callback{
             override fun onFailure(call: Call, e: IOException) {
                 println("Failed")
             }

             override fun onResponse(call: Call, response: Response) {
                 val body_comments = response.body?.string()
                  products_comments = Gson().fromJson(body_comments,Array<comments_model>::class.java
                 )
             }
         })
    }


    private fun fetchJson() {
       fetchComments()
        val url ="https://jsonplaceholder.typicode.com/posts"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback{


            override fun onFailure(call: Call, e: IOException) {
                println("Failed")
            }
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                products = Gson().fromJson(body,Array<models>::class.java)
                runOnUiThread {
                    initRecyclerView()
                    mAdapter = MyAdapter(this@MainActivity,products,products_comments,count)
                    recylclerView_main!!.adapter = mAdapter
                }
            }
        })
    }


}
