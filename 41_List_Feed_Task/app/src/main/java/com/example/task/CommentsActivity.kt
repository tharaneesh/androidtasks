package com.example.task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class CommentsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contents)
        var title = findViewById<TextView>(R.id.title_text_com)
        val id = findViewById<TextView>(R.id.id_text)
        var email = findViewById<TextView>(R.id.email_text)
        var body = findViewById<TextView>(R.id.body_text_com)
        var userId = findViewById<TextView>(R.id.user_id_text)
        val actionbar_title = intent.getStringExtra("actionBar_title")
        this.setTitle(actionbar_title)

        title.text=intent.getStringExtra("name")
        id.text=intent.getStringExtra("ID")
        email.text=intent.getStringExtra("email")
        body.text=intent.getStringExtra("body")
        userId.text=intent.getStringExtra("user_id")
    }
}