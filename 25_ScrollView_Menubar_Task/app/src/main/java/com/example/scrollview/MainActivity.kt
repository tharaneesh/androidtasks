package com.example.scrollview


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.getItemId()) {
            R.id.Edit_text -> {
                val edit:Toast=Toast.makeText(this,"Edit choice clicked",Toast.LENGTH_SHORT)
                edit.show()
                true
            }
            R.id.share_text -> {
                displayToast("Share choice clicked.")
                true
            }
            R.id.delete_text -> {
                displayToast("Delete choice clicked.")
                true
            }
            else -> return true
        }
    }
    fun displayToast(message: String?) {
        Toast.makeText(applicationContext, message,
                Toast.LENGTH_SHORT).show()
    }
    fun Show_comment(view: View) {
        val comment: Toast = Toast.makeText(this, R.string.msg,
                Toast.LENGTH_SHORT)
        comment.show()
    }
}