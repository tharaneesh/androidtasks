package com.example.myapplication



import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class OrderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        val spinner = findViewById<Spinner>(R.id.label_spinner)
        val text_message=findViewById<TextView>(R.id.textView_spinner)

        spinner.onItemSelectedListener =object :  AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Toast.makeText(this@OrderActivity,"You Selected ${adapterView?.getItemAtPosition(position).toString()}",
                        Toast.LENGTH_LONG).show()
                text_message.text="${adapterView?.getItemAtPosition(position).toString()}"

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }


        // Get the intent and its data.
        val intent = intent
        val message = intent.getStringExtra(MainActivity().EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.order_textview)
        textView.text = message
        val button=findViewById<Button>(R.id.button_back)
        button.setOnClickListener {
            finish()
        }
    }
    fun displayToast(message: String?) {
        Toast.makeText(applicationContext, message,
                Toast.LENGTH_SHORT).show()
    }

    fun onRadioButtonClicked(view: View) {
        val checked = (view as RadioButton).isChecked
        // Check which radio button was clicked.
        when (view.getId()) {
            R.id.sameday -> if (checked) // Same day service
                displayToast(getString(R.string.same_day_messenger_service))
            R.id.nextday -> if (checked) // Next day delivery
                displayToast(getString(R.string.next_day_ground_delivery))
            R.id.pickup -> if (checked) // Pick up
                displayToast(getString(R.string.pick_up))
            else -> {
            }
        }

    }
}