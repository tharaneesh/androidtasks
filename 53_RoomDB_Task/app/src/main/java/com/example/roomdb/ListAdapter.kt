package com.example.roomdb

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.ListFragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdb.ListAdapter.MyViewHolder
import com.example.roomdb.fragment.list.ListFragmentDirections
import com.example.roomdb.model.User
import kotlinx.android.synthetic.main.custom_layout.view.*

class ListAdapter: RecyclerView.Adapter<MyViewHolder>() {
    private var userList = emptyList<User>()

    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_layout,parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val currentItem = userList[position]
        holder.itemView.id_txt.text = currentItem.id.toString()
        holder.itemView.firstName_txt.text=currentItem.firstName
        holder.itemView.lastName_txt.text=currentItem.lastName
        holder.itemView.age_txt.text=currentItem.age.toString()

        holder.itemView.rowLayout.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment2(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }
    fun setData(user:List<User>){
        this.userList=user
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
    return userList.size
    }
}