package com.example.notificationtask

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RemoteViews
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat


class MainActivity : AppCompatActivity() {
     val PRIMARY_CHANNEL_ID = "primary_notification_channel"
     var mNotifyManager: NotificationManager? = null
     val NOTIFICATION_ID = 0
     var button_cancel: Button? = null
     var button_update: Button? = null
     var button_notify: Button? = null
    private val ACTION_UPDATE_NOTIFICATION =
        "com.android.example.notifyme.ACTION_UPDATE_NOTIFICATION"
    private val mReceiver = NotificationReceiver()
  //  private var notificationManager: NotificationManagerCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        registerReceiver(mReceiver,
             IntentFilter(ACTION_UPDATE_NOTIFICATION)
        )
     //   notificationManager = NotificationManagerCompat.from(this);

        button_cancel=findViewById(R.id.cancel)
        button_update=findViewById(R.id.update)
        button_notify=findViewById(R.id.notify)

      //  button_notify!!.isEnabled=false
        button_notify!!.setOnClickListener{
            val updateIntent = Intent(ACTION_UPDATE_NOTIFICATION)
            val updatePendingIntent = PendingIntent.getBroadcast(
                this,
                NOTIFICATION_ID,
                updateIntent,
                PendingIntent.FLAG_ONE_SHOT
            )

            val notifyBuilder = getNotificationBuilder()
            notifyBuilder!!.addAction(R.drawable.ic_update, "Update Notification", updatePendingIntent);

            mNotifyManager!!.notify(NOTIFICATION_ID, notifyBuilder!!.build())
            setNotificationButtonState(false,true,true)

        }
         button_update!!.setOnClickListener{

             updateNotification()
             setNotificationButtonState(false, false, true);

         }

        button_cancel!!.setOnClickListener {
            mNotifyManager!!.cancel(NOTIFICATION_ID)
            setNotificationButtonState(true, false, false);

        }

    }

    fun updateNotification() {
        val notifyBuilder = getNotificationBuilder()
        notifyBuilder!!.setContentText("Updated notification")
            .setContentTitle("Android Task")
            .setAutoCancel(true)
            .setColor(Color.BLUE)
            .setSmallIcon(R.drawable.ic_notification_updated)
        if (notifyBuilder != null) {
            mNotifyManager!!.notify(NOTIFICATION_ID, notifyBuilder.build())
        }    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
        super.onDestroy()
    }


    fun createNotificationChannel() {
        mNotifyManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                PRIMARY_CHANNEL_ID,
                "Channel 1", NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.setDescription("Notification Channel 1");
            mNotifyManager!!.createNotificationChannel(notificationChannel);

        }

    }


     fun getNotificationBuilder(): NotificationCompat.Builder? {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val notificationPendingIntent = PendingIntent.getActivity(
            this,
            NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )
         val notifyBuilder = NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
             .setColor(Color.GREEN)
            .setContentTitle("You've been notified!")
            .setContentText("This is your notification text.")
            .setSmallIcon(R.drawable.ic_notification)
             .setStyle(NotificationCompat.InboxStyle()
                 .addLine("This is line1")
                 .addLine("This is line2")
                 .addLine("This is line3")
                 .addLine("This is line4")
                 .addLine("This is line5")
                 .setBigContentTitle("Content Title")
                 .setSummaryText("Summary Text"))
             .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.mascot_1))
            .setContentIntent(notificationPendingIntent)

        return notifyBuilder;

    }


   inner class NotificationReceiver() : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            updateNotification()
            setNotificationButtonState(false, false, true);

        }

    }

    fun setNotificationButtonState(
        isNotifyEnabled: Boolean?,
        isUpdateEnabled: Boolean?,
        isCancelEnabled: Boolean?
    ) {
        button_notify!!.isEnabled = isNotifyEnabled!!
        button_update!!.isEnabled = isUpdateEnabled!!
        button_cancel!!.isEnabled = isCancelEnabled!!
    }

    fun showNotification(view: View) {

        val collapsedView = RemoteViews(
            packageName,
            R.layout.notification_layout
        )
        val expandedView = RemoteViews(
            packageName,
            R.layout.expanded
        )
        // this intent for notificationreceiver class for clicking the image we can show toast
//        val clickIntent = Intent(this, NotificationReceiver::class.java)
//        //getactivity/getbroadcast
//        val clickPendingIntent = PendingIntent.getBroadcast(
//            this,
//            0, clickIntent, 0
//        )
        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "Custom notification!")
       expandedView.setImageViewResource(R.id.image_view_expanded, R.drawable.mascot_1)
      //  expandedView.setOnClickPendingIntent(R.id.image_view_expanded, clickPendingIntent)

        //use also extends Notification
        val notification= NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_update)
            .setCustomContentView(collapsedView)
            .setCustomBigContentView(expandedView)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())

        mNotifyManager!!.notify(NOTIFICATION_ID, notification.build());
    }


}




//class MainActivity : AppCompatActivity() {
//    var mbutton : Button? =null
//    private var button_cancel: Button? = null
//    private var button_update: Button? = null
//
//    lateinit var notificationManager: NotificationManager
//    lateinit var notificationChannel: NotificationChannel
//    lateinit var builder: Notification.Builder
//    private val channelId="com.example.notificationtask"
//    private val description = "Test notification"
//
//    @RequiresApi(Build.VERSION_CODES.O)
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//         mbutton=findViewById(R.id.notify)
//        button_cancel=findViewById(R.id.cancel)
//        button_update=findViewById(R.id.update)
//
//      button_update!!.setOnClickListener{
//          val androidImage = BitmapFactory
//              .decodeResource(resources, R.drawable.mascot_1)
//
//      }
//        button_cancel!!.setOnClickListener {
//            notificationManager.cancel(1234)
//        }
//
//        mbutton!!.setOnClickListener {
//            val intent = Intent(this,MainActivity::class.java)
//            val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
//
//            val contentView = RemoteViews(packageName,R.layout.notification_layout)
//            contentView.setTextViewText(R.id.text,"Android_Task")
//            contentView.setTextViewText(R.id.title,"Text notification")
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                notificationChannel =
//                    NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
//                notificationChannel.enableLights(true)
//                notificationChannel.lightColor = Color.GREEN
//                notificationChannel.enableVibration(false)
//                notificationManager.createNotificationChannel(notificationChannel)
//
//                builder = Notification.Builder(this,channelId)
//                    .setContent(contentView)
//                        // setContent or this
//                   // .setContentTitle("AndroidTask")
//                   // .setContentText("Test Notification")
//                    .setSmallIcon(R.drawable.ic_notification)
//                    .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.ic_notification))
//                    .setContentIntent(pendingIntent)
//            }
//            //else for API less than 26
//            else
//            {
//                builder = Notification.Builder(this)
//                    .setContentTitle("AndroidTask")
//                    .setContentText("Test Notification")
//                    .setSmallIcon(R.drawable.ic_launcher_background)
//                    .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.ic_launcher_background))
//                    .setContentIntent(pendingIntent)
//            }
//            notificationManager.notify(1234,builder.build())
//        }
//    }
//}
