@file:Suppress("DEPRECATION")

package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.preference.*
import android.text.TextUtils
import android.view.MenuItem


class SecondActivity : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  fragmentManager.beginTransaction().replace(android.R.id.content, NotificationPreferenceFragment()).commit()
    }

    override fun onBuildHeaders(target: List<Header>) {
        super.onBuildHeaders(target)
        println("onBuild")
       loadHeadersFromResource(R.xml.pref_headers, target)
    }
    override fun isValidFragment(fragmentName: String): Boolean {
        println("isValidFragment")
        return PreferenceFragment::class.java.name == fragmentName || GeneralPreferenceFragment::class.java.name == fragmentName || DataSyncPreferenceFragment::class.java.name == fragmentName || NotificationPreferenceFragment::class.java.name == fragmentName

    }

    class DataSyncPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences_data)
            setHasOptionsMenu(true)

             bindPreferenceSummaryToValue(findPreference("sync_frequency"))
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val id: Int = item.getItemId()
            if (id == R.id.home) {
                startActivity(
                    Intent(activity, SecondActivity::class.java))
            }
            return super.onOptionsItemSelected(item)
        }
    }
    class GeneralPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences_general)
            setHasOptionsMenu(true)

            bindPreferenceSummaryToValue(findPreference("example_text"))
            bindPreferenceSummaryToValue(findPreference("example_list"))
            bindPreferenceSummaryToValue(findPreference("example_del_list"))
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val id = item.itemId
            if (id == android.R.id.home) {
                startActivity(
                    Intent(activity, SecondActivity::class.java)
                )
                return true
            }
            return super.onOptionsItemSelected(item)
        }
    }
    class NotificationPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_notification)
            setHasOptionsMenu(true)

            bindPreferenceSummaryToValue(
                findPreference("notifications_new_message_ringtone")
            )
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val id = item.itemId
            if (id == android.R.id.home) {
                startActivity(
                    Intent(activity, SecondActivity::class.java)
                )
                return true
            }
            return super.onOptionsItemSelected(item)
        }
    }
    companion object {
        private val TAG = SecondActivity::class.java.getSimpleName()

        private fun bindPreferenceSummaryToValue(preference: Preference) {
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                    .getDefaultSharedPreferences(preference.context)
                    .getString(preference.key, ""))
        }

        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, newValue ->
            val stringValue = newValue.toString()

            if (preference is ListPreference) {
                val index = preference.findIndexOfValue(stringValue)
                preference.setSummary(
                    if (index >= 0) {
                        preference.entries[index]
                    }
                    else
                        null)

            }
            else if (preference is RingtonePreference) {
                if (TextUtils.isEmpty(stringValue)) {
                    preference.setSummary(R.string.pref_ringtone_silent)
                } else {
                    val ringtone = RingtoneManager.getRingtone(
                        preference.getContext(), Uri.parse(stringValue)
                    )
                    if (ringtone == null) {
                        preference.setSummary(null)
                    } else {
                        val name = ringtone.getTitle(preference.getContext())
                        preference.setSummary(name)
                    }
                }
            }
            else
            {
                preference.summary = stringValue
            }
            true
        }
    }

    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }
    private fun isXLargeTablet(context: Context): Boolean {
        return ((context.getResources().getConfiguration().screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_XLARGE)
    }

}