package com.example.myapplication

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    val EXTRA_MESSAGE = "com.example.android.myapplication.extra.MESSAGE"
    private val mOrderMessage: String? = null
    private var mOrderMessage_donut: TextView? = null
    private var mOrderMessage_Froyo: TextView? = null
    private var mOrderMessage_ice: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity,
                    OrderActivity::class.java)
           // intent.putExtra(EXTRA_MESSAGE, mOrderMessage)
            startActivity(intent)
        }
        setSupportActionBar(findViewById(R.id.toolbar))
        mOrderMessage_donut=findViewById(R.id.textView2)
        mOrderMessage_Froyo=findViewById(R.id.textView3)
        mOrderMessage_ice=findViewById(R.id.textView4)
//        androidx.preference.PreferenceManager
//            .setDefaultValues(this, R.xml.preferences_data, false);
//        val sharedPref: SharedPreferences = androidx.preference.PreferenceManager
//            .getDefaultSharedPreferences(this)
        PreferenceManager.setDefaultValues(
            this,
            R.xml.preferences_general, false
        )
        PreferenceManager.setDefaultValues(
            this,
            R.xml.pref_notification, false
        )
        PreferenceManager.setDefaultValues(
            this,
            R.xml.preferences_data, false
        )


        val sharedPref: SharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this)
        val marketPref = sharedPref
            .getString("sync_frequency", "-1")
        println(marketPref)
        Toast.makeText(this, marketPref,
            Toast.LENGTH_LONG).show()

    }




    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_order -> {
                displayToast("hello");
                return true

            }
            R.id.action_status -> {
                display_Toast(item.title.toString())
                return true
            }
            R.id.action_favorites -> {
                display_Toast(getString(R.string.action_favorites_message))
                return true
            }
            R.id.action_contact -> {
                display_Toast(getString(R.string.action_contact_message))
                return true
            }
            R.id.action_settings -> {
                val intent = Intent(this,SecondActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }



    fun showDonutOrder(view: View) {
        displayToast(getString(R.string.donut_order_message));
        val intent=Intent(this, OrderActivity::class.java)
        val message:String=(getString(R.string.donut_order_message))
        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent)

    }
    fun showFroyoOrder(view: View) {
        displayToast(getString(R.string.froyo_order_message));
        val intent=Intent(this, OrderActivity::class.java)
        val message:String=(getString(R.string.froyo_order_message))
        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent)

    }
    fun shoeIcecreamOrder(view: View) {
        displayToast(getString(R.string.ice_cream_order_message));
        val intent=Intent(this, OrderActivity::class.java)
        val message:String=(getString(R.string.ice_cream_order_message))
        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent)

    }
    fun displayToast(message: String?) {
        Toast.makeText(this, message,
                Toast.LENGTH_SHORT).show()
    }
    fun display_Toast(item: String) {
        Toast.makeText(this, item,
                Toast.LENGTH_SHORT).show()
    }

}