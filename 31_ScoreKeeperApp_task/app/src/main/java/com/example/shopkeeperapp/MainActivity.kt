package com.example.shopkeeperapp

import android.app.UiModeManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate


class MainActivity : AppCompatActivity() {
    var mScore1 = 0
    var mScore2 = 0
    var mTextView1 : TextView?=null
    var mTextView2 : TextView?=null
    var mUiModeManager : UiModeManager?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mTextView1=findViewById(R.id.team1_zero)
        mTextView2=findViewById(R.id.team2_zero)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        val nightMode = AppCompatDelegate.getDefaultNightMode()
        if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
            menu!!.findItem(R.id.night_mode).setTitle(R.string.day_mode)
        } else {
            menu!!.findItem(R.id.night_mode).setTitle(R.string.night_mode)
        }
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === R.id.night_mode) {
            // Get the night mode state of the app.
            val nightMode = AppCompatDelegate.getDefaultNightMode()
            // Set the theme mode for the restarted activity.
            if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
            // Recreate the activity for the theme change to take effect.
            recreate()
        }
        return true
    }

    fun dec_value(view: View) {
        val viewID = view.id
        when (viewID) {
            R.id.dec_team1 -> {
                mScore1--
                //  print(mScore1)
                mTextView1!!.setText(mScore1.toString())
            }
            R.id.dec_team2 -> {
                mScore2--
                mTextView2!!.setText(mScore2.toString())
            }
        }

    }
    fun add_value(view: View) {
        val viewID =view.id
        when(viewID)
        {
            R.id.add_team1 -> {
                mScore1++
                mTextView1!!.setText(mScore1.toString())
            }
            R.id.add_team2 -> {
                mScore2++
                mTextView2!!.setText(mScore2.toString())
            }
        }
    }

}