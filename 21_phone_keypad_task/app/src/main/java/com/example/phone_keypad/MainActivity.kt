package com.example.phone_keypad

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val editText = findViewById<EditText>(R.id.edit_text)

        editText?.setOnEditorActionListener { textView, actionId, keyEvent ->
                       var handled = false

            if (actionId == EditorInfo.IME_ACTION_SEND) {
                Call()
                handled = true
            }
            handled
        }

    }

    private fun Call() {
        val phone_num = findViewById<View>(R.id.edit_text) as EditText
        val uri_parse = Uri.parse("tel:" + phone_num.text.toString())
        val intent = Intent(Intent.ACTION_DIAL, uri_parse)
        startActivity(intent)
    }


    fun Call(view: View) {
        val phone_num = findViewById<View>(R.id.edit_text) as EditText
        Toast.makeText(this, "clicked", Toast.LENGTH_LONG)
            .show()
        val uri_parse = Uri.parse("tel:" + phone_num.text.toString())
        val intent = Intent(Intent.ACTION_DIAL, uri_parse)
        startActivity(intent)

    }
