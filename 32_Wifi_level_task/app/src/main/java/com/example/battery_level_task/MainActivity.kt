package com.example.battery_level_task

import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    var mAnimatedView : AnimationDrawable?=null
    var mImageView : ImageView?=null
    var mButton_plus : Button? = null
    var mButton_minus : Button? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mImageView=findViewById(R.id.image)
        mImageView!!.setBackgroundResource(R.drawable.animation)
        mAnimatedView = mImageView!!.getBackground() as AnimationDrawable?;
        mButton_minus=findViewById(R.id.button_minus)
        mButton_plus=findViewById(R.id.button_plus)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        mButton_plus!!.setOnClickListener{
            mAnimatedView!!.start()
        }
        mButton_minus!!.setOnClickListener {
            mAnimatedView!!.stop()
        }

    }
}