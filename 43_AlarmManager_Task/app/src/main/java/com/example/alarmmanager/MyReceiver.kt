package com.example.alarmmanager

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class MyReceiver : BroadcastReceiver() {
    private val PRIMARY_CHANNEL_ID = "primary_notification_channel"
    override fun onReceive(context: Context, intent: Intent) {

        Toast.makeText(context,"Inside Receiver", Toast.LENGTH_LONG).show()

        var mNotificationManager = NotificationManagerCompat.from(context)


        val contentIntent = Intent(context, ImageActivity::class.java)
        val NOTIFICATION_ID = 0
        val contentPendingIntent = PendingIntent.getActivity(
            context,
            NOTIFICATION_ID,
            contentIntent,
            0
        )

        val builder = NotificationCompat.Builder(context,PRIMARY_CHANNEL_ID)
            builder.setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Stand Up Alert")
            .setContentText("You should stand up and walk around now!")
           .setContentIntent(contentPendingIntent)
                .setAutoCancel(true)
        mNotificationManager.notify(0,builder.build())

    }
}