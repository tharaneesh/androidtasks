package com.example.alarmmanager

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    var alarmManager: AlarmManager? = null
    var mNotificationManager:NotificationManager?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
createNotificationChannel()
    }

    fun startAlarm(view: View) {

        Toast.makeText(this,"Alarm Started",Toast.LENGTH_LONG).show()
   val intent = Intent(this,MyReceiver::class.java)
        val notifyPendingIntent = PendingIntent.getBroadcast(
            this,0, intent,
            0)

        alarmManager!!.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),1000*300,notifyPendingIntent)
    }
    fun stopAlarm(view: View) {
        Toast.makeText(this,"Alarm Stopped",Toast.LENGTH_LONG).show()

        val intent = Intent(this,MyReceiver::class.java)
        val notifyPendingIntent = PendingIntent.getBroadcast(
            this,0, intent,
            0)
        mNotificationManager!!.cancelAll()
        alarmManager!!.cancel(notifyPendingIntent)
    }
    fun createNotificationChannel() {

       mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.O
        ) {

            val notificationChannel = NotificationChannel(
                "primary_notification_channel",
                "Stand up notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.description = "Notifies every 15 minutes to " +
                    "stand up and walk"
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
    }
}