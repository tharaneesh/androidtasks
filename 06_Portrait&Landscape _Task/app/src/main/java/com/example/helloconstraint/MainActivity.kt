package com.example.helloconstraint

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showToast(view: View) {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            Toast.makeText(
                this, R.string.p_msg,
                Toast.LENGTH_SHORT
            ).show()
        else {
            Toast.makeText(
                this, R.string.l_msg,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
