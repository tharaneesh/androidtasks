package com.example.simplejobtask2

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    var jobScheduler: JobScheduler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun scheduleJob(v: View?) {
        jobScheduler = getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler

        println("Start Job")
        val serviceName = ComponentName(
            packageName,
            ExampleJobService::class.java.name
        )
        val builder = JobInfo.Builder(101, serviceName)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_CELLULAR)
            .setPeriodic((15 * 60 * 1000).toLong())
            .setRequiresCharging(true)
            .setPersisted(true)
            .build()
        jobScheduler!!.schedule(builder)
        if(jobScheduler!!.schedule(builder)==JobScheduler.RESULT_SUCCESS) {
            println("Job Scheduled")
        }
    }

    fun cancelJob(v: View?) {
        val scheduler = getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler
        scheduler.cancel(101)
        Log.d(TAG, "Job cancelled")
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}